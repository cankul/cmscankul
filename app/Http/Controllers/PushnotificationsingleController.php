<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Laracasts\Flash\Flash;
use Illuminate\Auth\Access\Response;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use App\Models\Borrower;
use App\Models\User;
use App\Models\LoanProduct;
use App\Models\LoanDisbursedBy;

// use Illuminate\Support\Facades\Request;\
// use Illuminate\Http\Request;
use App\http\Requests;

class PushnotificationsingleController extends Controller
{
    public function __construct()
    {
        $this->middleware(['sentinel', 'branch']);
    }
    
    public function index()
    {
        $data = DB::table('push_notification_single')
                    ->get();
                    
        return view('pushnotificationsingle.data', compact('data'));
    }
    public function create(Request $request)
    {
        if (!Sentinel::hasAccess('inboxmessageall.create')) {
            Flash::warning("Permission Denied");
            return redirect('/');
        }
        $borrowers = array();
        foreach (Borrower::all() as $key) {
            $borrowers[$key->mobile] = $key->first_name . ' ' . $key->last_name . '(' . $key->unique_number . ')';
        }
        $users = [];
        foreach (User::all() as $key) {
            $users[$key->id] = $key->first_name . ' ' . $key->last_name;
        }
        $loan_products = array();
        foreach (LoanProduct::all() as $key) {
            $loan_products[$key->id] = $key->name;
        }
        $loan_disbursed_by = array();
        foreach (LoanDisbursedBy::all() as $key) {
            $loan_disbursed_by[$key->id] = $key->name;
        }
        if (isset($request->product_id)) {
            $loan_product = LoanProduct::find($request->product_id);
        } else {
            $loan_product = LoanProduct::first();
        }
        if (isset($request->borrower_id)) {
            $borrower_id = $request->borrower_id;
        } else {
            $borrower_id = '';
        }
        // if (empty($loan_product)) {
        //     Flash::warning("No loan product set. You must first set a loan product");
        //     return redirect()->back();
        // }
        // dd($request->borrower_id);
        // print_r($request->borrower_id);
        // die();
        return view('pushnotificationsingle.create', compact('borrowers', 'loan_disbursed_by', 'loan_products', 'loan_product', 'borrower_id', 'custom_fields',
        'charges', 'loan_overdue_penalties', 'users'));
    }
    public function store(Request $request)
    {
        // $data = DB::table('borrowers_token')
        //                 ->where('mobile','=', $request->borrower_id[0])
        //                 ->get();
        // print_r($data[0]->token);
        // die();
        for ($i=0; $i < count($request->borrower_id) ; $i++) { 
            $token = DB::table('borrowers_token')
                        ->where('mobile','=', $request->borrower_id[$i])
                        ->get();

                        $data = [
                            'device_token' => $token[0]->token,
                            'mobile' => $request->borrower_id[$i],
                            'title' => $request->title,
                            'message' => $request->message,
                        ];
                        
                        $curl = curl_init();
                        
                        curl_setopt_array($curl, array(
                            CURLOPT_URL => "http://178.128.100.176/api.cangkul.com/notification/send/",
                            CURLOPT_RETURNTRANSFER => true,
                            CURLOPT_ENCODING => "",
                            CURLOPT_MAXREDIRS => 10,
                            CURLOPT_TIMEOUT => 30000,
                            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                            CURLOPT_CUSTOMREQUEST => "POST",
                            CURLOPT_POSTFIELDS => json_encode($data),
                            CURLOPT_HTTPHEADER => array(
                                // Set here requred headers
                                "accept: */*",
                                "accept-language: en-US,en;q=0.8",
                                "content-type: application/json",
                                "x-api-token:53FC78B2C7EC5D03F4FC7A3917F5AEB2",
                            ),
                        ));
                        
                        $response = curl_exec($curl);
                        $err = curl_error($curl);
                        
                        curl_close($curl);
                        
                        if ($err) {
                            Flash::success($err);
                        } else {
                            
                            $data = json_decode($response);
                            if ($data->status == false) {
                               
                                Flash::success($data->message);
                                return redirect('pushnotifsingle/data' );
                               
                            }else{
                               
                                $insert= DB::table('push_notification_single')->insert(
                                    [
                                        'mobile'=> $request->borrower_id[$i],
                                        'title' => $request->title,
                                        'message' =>  $request->message,
                                        'date_created' => date("Y-m-d : h:i:s" ),
                                    ]
                                );
                            }
                          
                            
                        }

           
        }

      
    
            if ($insert == "true") {
                 Flash::success("Successfully Saved");
                return redirect('pushnotifsingle/data' );
            }
    }
    public function delete($id)
    {
        $delete = DB::table('push_notification_single')->where('id', '=', $id)->delete();
        // dd($delete);
        Flash::success("Successfully Deleted");
        return redirect('pushnotifsingle/data');

    }
    public function edit($id)
    {

        
        $data = DB::table('push_notification')
                        ->where('id','=', $id)
                        ->get();
        // dd($data);
        return 
        view('pushnotificationsingle.edit', compact('data'));
        

    }
    public function update(Request $request, $id)
    {
        $update = DB::table('push_notification')->where('id',$id)->update([
            'title' => $request->title,
            'message'=> $request->message,
            'date'=> date("Y-m-d : h:i:s" )
        ]);
        // dd($update);
        if ($update = 1) {
            Flash::success('Successfully Saved');
            return redirect('pushnotifsingle/data');
        }

    }
  


    public function sendnotifborrower(Request $request)
    {
        $data = [
            'mobile' => '6285713460593',
            'title' => 'test dari laravel',
            'message' => 'test dari laravel',
        ];
        
        $curl = curl_init();
        
        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://178.128.100.176/api.cangkul.com/notification/send/",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_HTTPHEADER => array(
                // Set here requred headers
                "accept: */*",
                "accept-language: en-US,en;q=0.8",
                "content-type: application/json",
                "x-api-token:53FC78B2C7EC5D03F4FC7A3917F5AEB2",
            ),
        ));
        
        $response = curl_exec($curl);
        $err = curl_error($curl);
        
        curl_close($curl);
        
        if ($err) {
            Flash::success($err);
        } else {
            
            $data = json_decode($response);
            if ($data->status == false) {
                // echo "gagal";
                Flash::success($data->message);
                // return redirect('pushnotif/data');
            }else{
                // echo "berhasil";
                Flash::success($data->message);
            }
          
            
        }
    }
   

}
