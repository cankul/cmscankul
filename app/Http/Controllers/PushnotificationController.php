<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Laracasts\Flash\Flash;
use Illuminate\Auth\Access\Response;
// use Illuminate\Support\Facades\Request;\
// use Illuminate\Http\Request;
use App\http\Requests;

class PushnotificationController extends Controller
{
    
    public function index()
    {
        $data = DB::table('push_notification')
                    ->get();
                    
        return view('pushnotification.data', compact('data'));
    }
    public function create(Request $request)
    {
        return view('pushnotification.create');
    }
    public function store(Request $request)
    {
            $insert= DB::table('push_notification')->insert(
                [
                    'title' => $request->title,
                    'message' =>  $request->message,
                    'date' => date("Y-m-d : h:i:s" ),
                ]

            );

            if ($insert == "true") {
                 Flash::success("Successfully Saved");
                return redirect('pushnotif/data' );
            }
    }
    public function delete($id)
    {
        $delete = DB::table('push_notification')->where('id', '=', $id)->delete();
        Flash::success("Successfully Deleted");
        return redirect('pushnotif/data');

    }
    public function edit($id)
    {

        
        $data = DB::table('push_notification')
                        ->where('id','=', $id)
                        ->get();
        // dd($data);
        return 
        view('pushnotification.edit', compact('data'));
        

    }
    public function update(Request $request, $id)
    {
        $update = DB::table('push_notification')->where('id',$id)->update([
            'title' => $request->title,
            'message'=> $request->message,
            'date'=> date("Y-m-d : h:i:s" )
        ]);
        // dd($update);
        if ($update = 1) {
            Flash::success('Successfully Saved');
            return redirect('pushnotif/data');
        }

    }
    public function send($id)
    {
        
        $data = DB::table('push_notification')
        ->where('id','=', $id)
        ->get();
        
        $data = [
            'title' => $data[0]->title,
            'message' => $data[0]->message,
        ];
        // dd($data);
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://178.128.100.176/api.cangkul.com/notification/send/global/",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_HTTPHEADER => array(
                // Set here requred headers
                "accept: */*",
                "accept-language: en-US,en;q=0.8",
                "content-type: application/json",
                "x-api-token:53FC78B2C7EC5D03F4FC7A3917F5AEB2",
            ),
        ));
        
        $response = curl_exec($curl);
        $err = curl_error($curl);
        
        curl_close($curl);
        
        if ($err) {
            Flash::success("Successfully gagal");
            return redirect('pushnotif/data');
        } else {
            Flash::success("Successfully Send Notification");
            return redirect('pushnotif/data');
        }
    }


    public function sendnotifborrower(Request $request)
    {
        $data = [
            'mobile' => '6285713460593',
            'title' => 'test dari laravel',
            'message' => 'test dari laravel',
        ];
        
        $curl = curl_init();
        
        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://178.128.100.176/api.cangkul.com/notification/send/",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_HTTPHEADER => array(
                // Set here requred headers
                "accept: */*",
                "accept-language: en-US,en;q=0.8",
                "content-type: application/json",
                "x-api-token:53FC78B2C7EC5D03F4FC7A3917F5AEB2",
            ),
        ));
        
        $response = curl_exec($curl);
        $err = curl_error($curl);
        
        curl_close($curl);
        
        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            
            $data = json_decode($response);
            if ($data->status == false) {
                echo "gagal";
                Flash::success($data->message);
                // return redirect('pushnotif/data');
            }else{
                echo "berhasil";
                Flash::success($data->message);
            }
          
            
        }
    }
   

}
