<?php

namespace App\Http\Controllers;

use Aloha\Twilio\Twilio;
use App\Helpers\BulkSms;
use App\Models\Borrower;

use App\Models\CustomField;
use App\Models\CustomFieldMeta;
use App\Models\Expense;
use App\Models\ExpenseType;
use App\Models\LoanFee;
use App\Models\Setting;
use App\Models\User;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Clickatell\Api\ClickatellHttp;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Laracasts\Flash\Flash;

use Illuminate\Support\Facades\DB;

class LoanFeesAdminController extends Controller
{
    public function __construct()
    {
        $this->middleware(['sentinel', 'branch']);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::table('fees_administration')->get();


        return view('loan_fees_admin.data', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        // return view('loan_fee.create', compact(''));
        return view('loan_fees_admin.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

       $insert_data = DB::table('fees_administration')->insert([
                "presentation"=> $request->presentation,
                "name"=>$request->name
        ]);
        if($insert_data){
            Flash::success(trans('general.successfully_saved'));
            return redirect('loan/fees');
        }
       
    }


    public function show($loan_fee)
    {

    }


    public function edit($id)
    {   
        $loan_fee = DB::table('fees_administration')->where('id',$id)->first();
        // dd($loan_fee);
        return view('loan_fees_admin.edit', compact('loan_fee'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $update = DB::table('fees_administration')->where('id',$id)->update([
                "presentation"=>$request->presentation,
                "name"=>$request->name
        ]);

        Flash::success(trans('general.successfully_saved'));
        return redirect('loan/fees');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        // LoanFee::destroy($id);

        $delete = DB::table('fees_administration')->where('id','=',$id)->delete();

        Flash::success(trans('general.successfully_deleted'));
        return redirect('loan/fees');
    }

}
