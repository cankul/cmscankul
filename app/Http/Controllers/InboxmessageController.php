<?php

namespace App\Http\Controllers;
// namespace App\Models;

use App\Events\InterestWaived;
use App\Events\LoanTransactionUpdated;
use App\Events\RepaymentCreated;
use App\Events\RepaymentReversed;
use App\Events\RepaymentUpdated;
use App\Helpers\GeneralHelper;
use App\Helpers\Infobip;
use App\Helpers\RouteSms;
use App\Models\BankAccount;
use App\Models\Capital;
use App\Models\CustomField;
use App\Models\CustomFieldMeta;
use App\Models\Email;
use App\Models\Guarantor;
use App\Models\JournalEntry;
use App\Models\Loan;
use App\Models\LoanApplication;
use App\Models\LoanCharge;
use App\Models\LoanFee;
use App\Models\LoanFeeMeta;
use App\Models\LoanGuarantor;
use App\Models\LoanOverduePenalty;
use App\Models\LoanProduct;
use App\Models\LoanProductCharge;
use App\Models\LoanRepayment;
use App\Models\LoanRepaymentMethod;
use App\Models\LoanDisbursedBy;
use App\Models\Borrower;
use App\Models\LoanSchedule;
use App\Models\LoanTransaction;
use App\Models\Setting;
use App\Models\Sms;
use App\Models\User;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;

use App\Models\InboxMessage;
use App\Models\Tax;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;
use Laracasts\Flash\Flash;

class InboxmessageController extends Controller
{
    public function __construct()
    {
        $this->middleware(['sentinel', 'branch']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
           if (!Sentinel::hasAccess('inboxmessage')) {
            Flash::warning("Permission Denied");
            return redirect('/');
        }
        $data = DB::table('inbox_message')
                    ->where('all','=', '0')
                    ->get();
     
      // $borrowers= array();
      //   for ($i=0; $i < count($data) ; $i++) { 
            # code...
      
         $borrowers = DB::table('borrowers')
                    // ->where('id','=', $data[$i]->borrower_id)
                    ->get();
        // }

       // print_r($borrowers);
       // die();
        return view('inboxmessage.data', compact('data','borrowers'));
    }
    public function indexallborrower()
    {
        if (!Sentinel::hasAccess('inboxmessageall')) {
            Flash::warning("Permission Denied");
            return redirect('/');
        }
      
        $data = DB::table('inbox_message')
                    ->where('all','=', '1')
                    ->groupBy('message')
                    ->get();
        
        return view('inboxmessage.dataall', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if (!Sentinel::hasAccess('inboxmessage.create')) {
            Flash::warning("Permission Denied");
            return redirect('/');
        }
     $borrowers = array();
        foreach (Borrower::all() as $key) {
            $borrowers[$key->mobile] = $key->first_name . ' ' . $key->last_name . '(' . $key->mobile . ')';
        }
        $users = [];
        foreach (User::all() as $key) {
            $users[$key->id] = $key->first_name . ' ' . $key->last_name;
        }
        $loan_products = array();
        foreach (LoanProduct::all() as $key) {
            $loan_products[$key->id] = $key->name;
        }
        $loan_disbursed_by = array();
        foreach (LoanDisbursedBy::all() as $key) {
            $loan_disbursed_by[$key->id] = $key->name;
        }
        if (isset($request->product_id)) {
            $loan_product = LoanProduct::find($request->product_id);
        } else {
            $loan_product = LoanProduct::first();
        }
        if (isset($request->borrower_id)) {
            $mobile = $request->mobile;
        } else {
            $mobile = '';
        }
        if (empty($loan_product)) {
            Flash::warning("No loan product set. You must first set a loan product");
            return redirect()->back();
        }
        // dd($borrowers);
    
        return view('inboxmessage.create',  compact('borrowers', 'loan_disbursed_by', 'loan_products', 'loan_product', 'mobile', 'custom_fields',
                'charges', 'loan_overdue_penalties', 'users'));
    }
    public function createall(Request $request)
    {
        if (!Sentinel::hasAccess('inboxmessageall.create')) {
            Flash::warning("Permission Denied");
            return redirect('/');
        }
        $borrowers = array();
        foreach (Borrower::all() as $key) {
            $borrowers[$key->id] = $key->first_name . ' ' . $key->last_name . '(' . $key->unique_number . ')';
        }
        $users = [];
        foreach (User::all() as $key) {
            $users[$key->id] = $key->first_name . ' ' . $key->last_name;
        }
        $loan_products = array();
        foreach (LoanProduct::all() as $key) {
            $loan_products[$key->id] = $key->name;
        }
        $loan_disbursed_by = array();
        foreach (LoanDisbursedBy::all() as $key) {
            $loan_disbursed_by[$key->id] = $key->name;
        }
        if (isset($request->product_id)) {
            $loan_product = LoanProduct::find($request->product_id);
        } else {
            $loan_product = LoanProduct::first();
        }
        if (isset($request->borrower_id)) {
            $borrower_id = $request->borrower_id;
        } else {
            $borrower_id = '';
        }
        if (empty($loan_product)) {
            Flash::warning("No loan product set. You must first set a loan product");
            return redirect()->back();
        }
        
        return view('inboxmessage.createall',  compact('borrowers', 'loan_disbursed_by', 'loan_products', 'loan_product', 'borrower_id', 'custom_fields',
                'charges', 'loan_overdue_penalties', 'users'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    
          


         for ($i=0; $i < count($request->mobile) ; $i++) { 
            $token = DB::table('user_firebase_auth')
                        ->where('mobile','=', $request->mobile[$i])
                        ->get();
                        $data = [
                            'device_token' => $token[0]->device_token,
                            'mobile' => $request->mobile[$i],
                            'title' => $request->message_header,
                            'message' => $request->message,
                        ];

                        
                        $curl = curl_init();
                        
                        curl_setopt_array($curl, array(
                            CURLOPT_URL => config('app.url_api_cankul')."/notification/send/",
                            CURLOPT_RETURNTRANSFER => true,
                            CURLOPT_ENCODING => "",
                            CURLOPT_MAXREDIRS => 10,
                            CURLOPT_TIMEOUT => 30000,
                            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                            CURLOPT_CUSTOMREQUEST => "POST",
                            CURLOPT_POSTFIELDS => json_encode($data),
                            CURLOPT_HTTPHEADER => array(
                                // Set here requred headers
                                "accept: */*",
                                "accept-language: en-US,en;q=0.8",
                                "content-type: application/json",
                                "x-api-token:53FC78B2C7EC5D03F4FC7A3917F5AEB2",
                            ),
                        ));
                        
                        $response = curl_exec($curl);
                        $err = curl_error($curl);
                        
                        curl_close($curl);
                        
                        if ($err) {
                            Flash::success($err);
                            return redirect('inboxmessage/data' );
                        } else {
                            
                            $data = json_decode($response);
                            // print_r($data);
                            // die();
                            if ($data->status == false) {
                               
                                Flash::success($data->message);
                                return redirect('inboxmessage/data' );
                               
                            }else{
                                 $borrowers = DB::table('borrowers')
                                    ->where('mobile','=', $request->mobile[$i])
                                    ->get();
                                  
                                    for ($x=0; $x < count($borrowers) ; $x++) { 
                                        $insert= DB::table('inbox_message')->insert(
                                            [
                                              'borrower_id' => $borrowers[$x]->id,
                                              'message_header' =>  $request->message_header,
                                              'message' =>  $request->message,
                                              'date_created' => date("Y-m-d : h:i:s" ),
                                              'all' => 0,
                                            ]
                                        );
                                    }
                                   if ($insert == "true") {
                                         Flash::success("Successfully Saved");
                                        return redirect('inboxmessage/data' );
                                    }
                            }
                          
                            
                        }

           
        }
    }

     public function storeall(Request $request)
    {

  
    foreach (Borrower::all() as $key) {
      $x[] = array('id' => $key->id, );
    }
    for ($i=0; $i < count($x); $i++) {
        $insert= DB::table('inbox_message')->insert(
                [
                    'borrower_id' => $x[$i]['id'],
                    'message_header' =>  $request->message_header,
                    'message' =>  $request->message,
                    'date_created' => date("Y-m-d : h:i:s" ),
                    'all' => 1,
                    'id_group' => date("Y-m-dh:i:s" )."1",
                ]

            );
            }
            if ($insert == "true") {


            $data = [
                'title' => $request->message_header,
                'message' => $request->message,
            ];
        
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => config('app.url_api_cankul')."/notification/send/global/",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30000,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => json_encode($data),
                CURLOPT_HTTPHEADER => array(
                    // Set here requred headers
                    "accept: */*",
                    "accept-language: en-US,en;q=0.8",
                    "content-type: application/json",
                    "x-api-token:53FC78B2C7EC5D03F4FC7A3917F5AEB2",
                ),
            ));
            
            $response = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);
            $res = json_decode($response);
            // print_r($res);
            // die();
                 Flash::success("Successfully Saved and ".$res->message);
                return redirect('inboxmessage/indexallborrower' );
            }

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }


    public function edit($inboxmessage)
    {
        if (!Sentinel::hasAccess('inboxmessage.update')) {
            Flash::warning("Permission Denied");
            return redirect('/');
        }
       $borrowers = array();
        foreach (Borrower::all() as $key) {
            $borrowers[$key->id] = $key->first_name . ' ' . $key->last_name . '(' . $key->unique_number . ')';
        }


        return View::make('inboxmessage.edit', compact('inboxmessage', 'borrowers'));
    }

     public function editall($inboxmessage)
    {

        if (!Sentinel::hasAccess('inboxmessageall.update')) {
            Flash::warning("Permission Denied");
            return redirect('/');
        }
       dd($inboxmessage);
       // $borrowers = array();
       //  foreach (Borrower::all() as $key) {
       //      $borrowers[$key->id] = $key->first_name . ' ' . $key->last_name . '(' . $key->unique_number . ')';
       //  }


       //  return View::make('inboxmessage.editall', compact('inboxmessage', 'borrowers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!Sentinel::hasAccess('inboxmessage.update')) {
            Flash::warning("Permission Denied");
            return redirect('/');
        }


        $update = DB::table('inbox_message')
                    ->where('id',$id)
                    ->update(['message'=> $request->message,
                                'borrower_id'=>$request->borrower_id,
                            ]
                    );
       
        if ($update==1) {
           Flash::success("Successfully Saved");
             return redirect('inboxmessage/data');
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        if (!Sentinel::hasAccess('inboxmessage.delete')) {
            Flash::warning("Permission Denied");
            return redirect('/');
        }
        $delete = DB::table('inbox_message')->where('id','=', $id)->delete();
     
        
            Flash::success("Successfully Deleted");
            return redirect('inboxmessage/data');
        
      
    }
     public function deleteall($id)
    {

        if (!Sentinel::hasAccess('inboxmessageall.delete')) {
            Flash::warning("Permission Denied");
            return redirect('/');
        }
        $delete = DB::table('inbox_message')->where('message','=', $id)->delete();
      

            Flash::success("Successfully Deleted");
            return redirect('inboxmessage/indexallborrower');
        
    }
}
