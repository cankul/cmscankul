<?php

namespace App\Http\Controllers;

use Aloha\Twilio\Twilio;
use App\Helpers\BulkSms;
use App\Models\Borrower;

use App\Models\CustomField;
use App\Models\CustomFieldMeta;
use App\Models\Expense;
use App\Models\ExpenseType;
use App\Models\LoanFee;
use App\Models\Setting;
use App\Models\User;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Clickatell\Api\ClickatellHttp;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Laracasts\Flash\Flash;

use Illuminate\Support\Facades\DB;

class LoanDisbrusmentController extends Controller
{
    public function __construct()
    {
        $this->middleware(['sentinel', 'branch']);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // dd('test test');
        $data_borrower = array();
        $borrower =[];
        $data = DB::table('loan_disbrusment')->get();
        foreach($data as $data_loan){
            // echo $data_loan->loan_id;
             
                $loans =DB::table('loans')->where('id',$data_loan->loan_id)->get();
                foreach($loans as $loan){
                    // echo $loan->borrower_id;
                    $borrowers = DB::table('borrowers')->where('id',$loan->borrower_id)->get();
                        // foreach($borrowers as $borrower){
                        //     // echo $borrower->first_name;
                        //     $borrower[] = $data_loan->amount_disbrus;
                        //     $data_borrower[]=  $borrower;
                            
                        // }

                        for ($i=0; $i < count($borrowers); $i++) { 
                            // dd($borrowers[0]->first_name);
                            $data_borrower[$i]['amount_disbrus'] = $data_loan->amount_disbrus;
                            $data_borrower[$i]['first_name'] =  $borrowers[$i]->first_name;
                        }
                    // echo $loan->id;
                }
        }
        dd($data_borrower);
        dd($data);

        return view('loan_fees_admin.data', compact('data'));
    }

   

}
