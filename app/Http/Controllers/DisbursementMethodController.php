<?php

namespace App\Http\Controllers;

use App\Models\BankAccount;
use App\Models\Tax;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\View;
use Laracasts\Flash\Flash;

use Illuminate\Support\Facades\DB;

class DisbursementMethodController extends Controller
{
    public function __construct()
    {
        $this->middleware(['sentinel', 'branch']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // if (!Sentinel::hasAccess('capital')) {
        //     Flash::warning("Permission Denied");
        //     return redirect('/');
        // }
        // $data = BankAccount::all();
        $disbursement_method = DB::table('disbursement_method')->where('parent_id', 0)->get();

        return view('DisbursementMethod.data', compact('disbursement_method'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('DisbursementMethod.create', compact(''));
    }

    public function Subcreate($id)
    {
       
   
        $disbursement = DB::table('disbursement_method')->where('id', $id)->get();
        return view('DisbursementMethod.Subcreate', compact('disbursement'));
    }

    public function Substore(Request $request)
    {
        // print_r($request->parent_id);

        // dd($request);
        // print_r($request);
        // die();
        $insert = DB::table('disbursement_method')->insert([
                    'parent_id' => $request->parent_id,
                    'title'     => $request->title,
                    'step'     => $request->step,
                    'created_at'     => date("Y:m:d h:i:sa")
                ]);
        if ($insert == 1) {
            Flash::success("Successfully Saved");
           
        }else{
            Flash::success("Not Successfully Saved");
        }
        
        return redirect('bank/disb/'.$request->parent_id.'/show');

    }

        public function Subedit($id)
    {
        

        $disbursement_method = DB::table('disbursement_method')->where('id', $id)->get();
        $disbursement = DB::table('disbursement_method')->where('id', $disbursement_method[0]->parent_id)->get();
        return View::make('DisbursementMethod.Subedit', compact('disbursement_method','disbursement'))->render();
    }

    public function Subupdate(Request $request, $id)
    {
        // dd('test');
        $update = DB::table('disbursement_method')
                    ->where('id', $id)
                    ->update([
                        'title' => $request->title,
                        'step' => $request->step,
                        'updated_at' => date("Y:m:d h:i:sa"),
                        ]);
        if ($update == 1) {
            Flash::success("Successfully Saved");   
        }else{
            Flash::success("not Successfully Saved");
        }
        $disbursement_method = DB::table('disbursement_method')->where('id', $id)->get();

        return redirect('bank/disb/'.$disbursement_method[0]->parent_id.'/show'); 

    }

     public function Subdelete($id)
    {
        $disbursement_method = DB::table('disbursement_method')->where('id', $id)->get();
        

        $delete = DB::table('disbursement_method')
                        ->where('id',$id)
                        ->delete();
        if ($delete == 1) {
            Flash::success("Successfully Deleted");
        }else{
            Flash::success("Not Successfully Deleted");
        }

        return redirect('bank/disb/'.$disbursement_method[0]->parent_id.'/show'); 

    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $insert = DB::table('disbursement_method')->insert([
                    'parent_id' => 0,
                    'title'     => $request->title,
                    'step'     => $request->step,
                    'created_at'     => date("Y:m:d h:i:sa")
                ]);
        if ($insert == 1) {
            Flash::success("Successfully Saved");
           
        }else{
            Flash::success("Not Successfully Saved");
        }
        
        return redirect('bank/disb/data');

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $disbursement = DB::table('disbursement_method')->where('id', $id)->get();
        $disbursement_method = DB::table('disbursement_method')->where('parent_id', $id)->get();

        return View::make('DisbursementMethod.dataDisb', compact('disbursement_method','disbursement'))->render();
    }


    public function edit($id)
    {
        

        $disbursement_method = DB::table('disbursement_method')->where('id', $id)->get();
        return View::make('DisbursementMethod.edit', compact('disbursement_method'))->render();
    }


    public function update(Request $request, $id)
    {
        $update = DB::table('disbursement_method')
                    ->where('id', $id)
                    ->update([
                        'title' => $request->title,
                        'step' => $request->step,
                        'updated_at' => date("Y:m:d h:i:sa"),
                        ]);
        if ($update == 1) {
            Flash::success("Successfully Saved");   
        }else{
            Flash::success("not Successfully Saved");
        }
        return redirect('bank/disb/data');
    }

    public function delete($id)
    {

        $delete = DB::table('disbursement_method')
                        ->where('id',$id)
                        ->delete();
        if ($delete == 1) {
            Flash::success("Successfully Deleted");
        }else{
            Flash::success("Not Successfully Deleted");
        }

        return redirect('bank/disb/data');
        
    }
    public function active($id)
    {
        // echo "test".$id;
        $active =DB::table('disbursement_method')->where('id',$id)->update([
            'status' => 1
        ]);
        if ($active == 1) {
            Flash::success("Successfully Saved");   
        }else{
            Flash::success("not Successfully Saved");
        }
        return redirect('bank/disb/data');
    }

    public function nonactive($id)
    {
        $active =DB::table('disbursement_method')->where('id',$id)->update([
            'status' => 0
        ]);
        if ($active == 1) {
            Flash::success("Successfully update nonactive");   
        }else{
            Flash::success("not Successfully update nonactive");
        }
        return redirect('bank/disb/data');
    }
}
