<?php

namespace App\Http\Controllers;

use Aloha\Twilio\Twilio;
use App\Helpers\GeneralHelper;
use App\Models\Borrower;
use App\Models\Lender;

use App\Models\Country;
use App\Models\CustomField;
use App\Models\CustomFieldMeta;
use App\Models\Loan;
use App\Models\LoanRepayment;
use App\Models\Setting;
use App\Models\User;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Clickatell\Api\ClickatellHttp;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Laracasts\Flash\Flash;

use App\Models\Saving;
use App\Models\SavingsProductCharge;
use App\Models\SavingProduct;
use App\Models\SavingTransaction;

use Illuminate\Support\Facades\DB;

class LenderController extends Controller
{
    public function __construct()
    {
        $this->middleware(['sentinel', 'branch']);

    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!Sentinel::hasAccess('borrowersmenu')) {
            Flash::warning("Permission Denied");
            return redirect('/');
        }
        $data = Lender::get();

        return view('lender.data', compact('data'));
    }

    public function pending()
    {
        if (!Sentinel::hasAccess('borrowersmenu')) {
            Flash::warning("Permission Denied");
            return redirect('/');
        }
        $data = Lender::where('branch_id', session('branch_id'))->where('active', 0)->get();

        return view('lender.pending', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!Sentinel::hasAccess('borrowersmenu.create')) {
            Flash::warning("Permission Denied");
            return redirect('/');
        }
        $users = User::all();
        $user = array();
        foreach ($users as $key) {
            $user[$key->id] = $key->first_name . ' ' . $key->last_name;
        }
        $countries = array();
        foreach (Country::all() as $key) {
            $countries[$key->id] = $key->name;
        }
        //get custom fields
        $custom_fields = CustomField::where('category', 'lender')->get();
        return view('lender.create', compact('user', 'custom_fields','countries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!Sentinel::hasAccess('borrowersmenu.create')) {
            Flash::warning("Permission Denied");
            return redirect('/');
        }
        $lender = new Lender();
        $lender->first_name = $request->first_name;
        $lender->last_name = $request->last_name;
        $lender->user_id = Sentinel::getUser()->id;
        $lender->gender = $request->gender;
        $lender->country_id = $request->country_id;
        $lender->title = $request->title;
        $lender->branch_id = session('branch_id');
        $lender->mobile = $request->mobile;
        $lender->notes = $request->notes;
        $lender->email = $request->email;
        if ($request->hasFile('photo')) {
            $file = array('photo' => Input::file('photo'));
            $rules = array('photo' => 'required|mimes:jpeg,jpg,bmp,png');
            $validator = Validator::make($file, $rules);
            if ($validator->fails()) {
                Flash::warning(trans('general.validation_error'));
                return redirect()->back()->withInput()->withErrors($validator);
            } else {
                $fname = "borrower_" . uniqid() .'.'. $request->file('photo')->guessExtension();
                $lender->photo = $fname;
                $request->file('photo')->move(public_path() . '/uploads',
                    $fname);
            }

        }
        $lender->unique_number = $request->unique_number;
        $lender->dob = $request->dob;
        $lender->address = $request->address;
        $lender->city = $request->city;
        $lender->state = $request->state;
        $lender->zip = $request->zip;
        $lender->phone = $request->phone;
        $lender->business_name = $request->business_name;
        $lender->working_status = $request->working_status;
        $lender->loan_officers = serialize($request->loan_officers);
        $date = explode('-', date("Y-m-d"));
        $lender->year = $date[0];
        $lender->month = $date[1];
        $files = array();
        if (!empty($request->file('files'))) {
            $count = 0;
            foreach ($request->file('files') as $key) {
                $file = array('files' => $key);
                $rules = array('files' => 'required|mimes:jpeg,jpg,bmp,png,pdf,docx,xlsx');
                $validator = Validator::make($file, $rules);
                if ($validator->fails()) {
                    Flash::warning(trans('general.validation_error'));
                    return redirect()->back()->withInput()->withErrors($validator);
                } else {
                    $fname = "lender_" . uniqid() .'.'. $key->guessExtension();
                    $files[$count] = $fname;
                    $key->move(public_path() . '/uploads',
                        $fname);
                }
                $count++;
            }
        }
        $lender->files = serialize($files);
        $lender->username = $request->username;
        if (!empty($request->password)) {
            $rules = array(
                'repeatpassword' => 'required|same:password',
                'username' => 'required|unique:borrowers'
            );
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
                Flash::warning('Passwords do not match');
                return redirect()->back()->withInput()->withErrors($validator);

            } else {
                $lender->password = md5($request->password);
            }
        }
        $lender->save();
        $custom_fields = CustomField::where('category', 'lenders')->get();
        foreach ($custom_fields as $key) {
            $custom_field = new CustomFieldMeta();
            $id = $key->id;
            $custom_field->name = $request->$id;
            $custom_field->parent_id = $lender->id;
            $custom_field->custom_field_id = $key->id;
            $custom_field->category = "lenders";
            $custom_field->save();
        }
        GeneralHelper::audit_trail("Added lender  with id:" . $lender->id);
        Flash::success(trans('general.successfully_saved'));
        return redirect('lender/data');
    }


    public function show($lender)
    {
        if (!Sentinel::hasAccess('borrowersmenu.view')) {
            Flash::warning("Permission Denied");
            return redirect('/');
        }
        $users = User::all();
        $user = array();
        foreach ($users as $key) {
            $user[$key->id] = $key->first_name . ' ' . $key->last_name;
        }
        //get custom fields
        $custom_fields = CustomFieldMeta::where('category', 'borrowers')->where('parent_id', $lender->id)->get();
        $note_decline = json_decode($lender->note_decline);
        $note_blacklist = json_decode($lender->note_blacklist);
        $note= array();
        $note_b= array();
        if ($lender->active == 0 && $lender->note_decline != "") {
            foreach ($note_decline as $key => $value) {
                if ($key && $value == "true" ) {
                    $note = DB::table('Note_decline')->where('slug',$key)->get();
                  
                }
            }
        }
        

        if ($lender->blacklisted == 1 && $lender->note_blacklist != "") {
            foreach ($note_blacklist as $key => $value) {
                if ( $value == true ) {
                    $note_b[] = DB::table('Note_blacklist')->where('slug',$key)->get();
                }
            }
        }

        // dd($custom_fields);
        $files = DB::table('Files')->where('mobile',$lender->mobile)->get();
        $SavingTransaction = SavingTransaction::where('borrower_id', $lender->id)->where('reversal_type','none')->get();
        // dd($SavingTransaction);
        $saving = Saving::where('borrower_id', $lender->id)->get();
        // dd($saving);
        return view('lender.show', compact('lender', 'user', 'custom_fields','note','note_b','files','SavingTransaction','saving'));
    }


    public function edit($lender)
    {

        if (!Sentinel::hasAccess('borrowersmenu.update')) {
            Flash::warning("Permission Denied");
            return redirect('/');
        }
        $users = User::all();
        $user = array();
        foreach ($users as $key) {
            $user[$key->id] = $key->first_name . ' ' . $key->last_name;
        }
        $countries = array();
        foreach (Country::all() as $key) {
            $countries[$key->id] = $key->name;
        }
        //get custom fields
        $custom_fields = CustomField::where('category', 'borrowers')->get();
        return view('lender.edit', compact('lender', 'user', 'custom_fields','countries'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request);
        if (!Sentinel::hasAccess('borrowersmenu.update')) {
            Flash::warning("Permission Denied");
            return redirect('/');
        }
        $lender = Lender::find($id);
        $lender->first_name = $request->first_name;
        $lender->last_name = $request->last_name;
        $lender->gender = $request->gender;
        $lender->country_id = $request->country_id;
        $lender->title = $request->title;
        $lender->mobile = $request->mobile;
        $lender->notes = $request->notes;
        $lender->email = $request->email;
        if ($request->hasFile('photo')) {
            $file = array('photo' => Input::file('photo'));
            $rules = array('photo' => 'required|mimes:jpeg,jpg,bmp,png');
            $validator = Validator::make($file, $rules);
            if ($validator->fails()) {
                Flash::warning(trans('general.validation_error'));
                return redirect()->back()->withInput()->withErrors($validator);
            } else {
                $fname = "lender_" . uniqid().'.'.$request->file('photo')->guessExtension();
                $lender->photo = $fname;
                $request->file('photo')->move(public_path() . '/uploads',
                    $fname);
            }

        }
        $lender->unique_number = $request->unique_number;
        $lender->dob = $request->dob;
        $lender->address = $request->address;
        $lender->city = $request->city;
        $lender->state = $request->state;
        $lender->zip = $request->zip;
        $lender->phone = $request->phone;
        $lender->business_name = $request->business_name;
        $lender->working_status = $request->working_status;
        $lender->loan_officers = serialize($request->loan_officers);
        $files = unserialize($lender->files);
        $count = count($files);
        if (!empty($request->file('files'))) {
            foreach ($request->file('files') as $key) {
                $count++;
                $file = array('files' => $key);
                $rules = array('files' => 'required|mimes:jpeg,jpg,bmp,png,pdf,docx,xlsx');
                $validator = Validator::make($file, $rules);
                if ($validator->fails()) {
                    Flash::warning(trans('general.validation_error'));
                    return redirect()->back()->withInput()->withErrors($validator);
                } else {
                    $fname = "lender_" . uniqid() .'.'. $key->guessExtension();
                    $files[$count] = $fname;
                    $key->move(public_path() . '/uploads',
                        $fname);
                }

            }
        }
        $lender->files = serialize($files);
        $lender->username = $request->username;
        if (!empty($request->password)) {
            $rules = array(
                'repeatpassword' => 'required|same:password'
            );
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
                Flash::warning('Passwords do not match');
                return redirect()->back()->withInput()->withErrors($validator);

            } else {
                $lender->password = md5($request->password);
            }
        }
        $lender->save();
        $custom_fields = CustomField::where('category', 'lenders')->get();
        foreach ($custom_fields as $key) {
            if (!empty(CustomFieldMeta::where('custom_field_id', $key->id)->where('parent_id', $id)->where('category',
                'lenders')->first())
            ) {
                $custom_field = CustomFieldMeta::where('custom_field_id', $key->id)->where('parent_id',
                    $id)->where('category', 'lenders')->first();
            } else {
                $custom_field = new CustomFieldMeta();
            }
            $kid = $key->id;
            $custom_field->name = $request->$kid;
            $custom_field->parent_id = $id;
            $custom_field->custom_field_id = $key->id;
            $custom_field->category = "lenders";
            $custom_field->save();
        }
        GeneralHelper::audit_trail("Updated lender  with id:" . $lender->id);
        Flash::success(trans('general.successfully_saved'));
        return redirect('lender/data');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        if (!Sentinel::hasAccess('borrowersmenu.delete')) {
            Flash::warning("Permission Denied");
            return redirect('/');
        }
        Lender::destroy($id);
        Loan::where('borrower_id', $id)->delete();
        LoanRepayment::where('borrower_id', $id)->delete();
        GeneralHelper::audit_trail("Deleted lender  with id:" . $id);
        Flash::success(trans('general.successfully_deleted'));
        return redirect('lender/data');
    }

    public function deleteFile(Request $request, $id)
    {
        if (!Sentinel::hasAccess('borrowersmenu.delete')) {
            Flash::warning("Permission Denied");
            return redirect('/');
        }
        $lender = Lender::find($id);
        $files = unserialize($lender->files);
        @unlink(public_path() . '/uploads/' . $files[$request->id]);
        $files = array_except($files, [$request->id]);
        $lender->files = serialize($files);
        $lender->save();


    }

    public function approve(Request $request, $id)
    {
        if (!Sentinel::hasAccess('borrowersmenu.approve')) {
            Flash::warning("Permission Denied");
            return redirect('/');
        }
        $lender = Lender::find($id);
        $lender->active = 1;
        $lender->note_decline = "";
        $lender->save();
        GeneralHelper::audit_trail("Approved lender  with id:" . $lender->id);
        Flash::success(trans('general.successfully_saved'));
        return redirect()->back();
    }

    public function decline(Request $request, $id)
    {   
        $note_decline = array(  'd1' =>  $request["d1"] ,
                                'd2' =>  $request["d2"] ,
                                'd3' =>  $request["d3"] ,
                                'd4' =>  $request["d4"] ,
                                'd5' =>  $request["d5"] ,

         );
        // print_r(json_encode($note_decline));
        // dd($request);
        

        if (!Sentinel::hasAccess('borrowersmenu.approve')) {
            Flash::warning("Permission Denied");
            return redirect('/');
        }
        $lender = Lender::find($id);
        $lender->active = 0;
        $lender->note_decline = json_encode($note_decline);
        $lender->save();
        GeneralHelper::audit_trail("Declined lender  with id:" . $lender->id);
        Flash::success(trans('general.successfully_saved'));
        return redirect()->back();
    }
    public function blacklist(Request $request, $id)
    {

        $blacklist = array( 'b1' => $request["b1"],
                            'b2' => $request["b2"],
                            'b3' => $request["b3"],
                             );
        if (!Sentinel::hasAccess('borrowersmenu.blacklist')) {
            Flash::warning("Permission Denied");
            return redirect('/');
        }
        $lender = Lender::find($id);
        $lender->blacklisted = 1;
        $lender->active = 0;
        $lender->note_blacklist = json_encode($blacklist);
        $lender->save();
        GeneralHelper::audit_trail("Blacklisted lender  with id:" . $id);
        Flash::success(trans('general.successfully_saved'));
        return redirect()->back();
    }

    public function unBlacklist(Request $request, $id)
    {
        if (!Sentinel::hasAccess('borrowersmenu.blacklist')) {
            Flash::warning("Permission Denied");
            return redirect('/');
        }
        $lender = Lender::find($id);
        $lender->blacklisted = 0;
        $lender->active = 1;
        $lender->note_blacklist = "";
        $lender->save();
        GeneralHelper::audit_trail("Undo Blacklist for lender  with id:" . $id);
        Flash::success(trans('general.successfully_saved'));
        return redirect()->back();
    }
}
