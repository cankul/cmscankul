<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Mail;
class CronSms extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sms:sendReminderRepayment';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Reminder Repayment to loan';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $loans = DB::table('loans')->where('status','disbursed')->get();
        foreach($loans as $loan){
            $loan->borrower_id;
            $borrowers = DB::table('borrowers')->where('id',$loan->borrower_id)->get();
            foreach($borrowers as $borrower){    
               


                $datetime = new \DateTime($loan->maturity_date);
                $datetime->modify('-1 day');
                $due_date = $datetime->format('Y-m-d H:i:s');
                $datenow = date('Y-m-d 00:00:00');
                if ($due_date == $datenow){
                    // SMS
                        $penalty =number_format(\App\Helpers\GeneralHelper::loan_paid_items($loan->id)['penalty'],2);
                        $balance = number_format(\App\Helpers\GeneralHelper::loan_total_balance($loan->id),2);
                        $message = "( Silahkan Lakukan Pembayaran Pinjaman Anda Sebelum  Tanggal ( {$due_date} )
                        Sebesar Rp. ( {$balance} ). Jikas Setelah Tanggal ( {$due_date} )
                        belum melakukan pelunasan maka anda akan dikenakan denda sebesar Rp. ( {$penalty} )
                        Terima Kasih.";
                        self::__Send_Short_Message($borrower->mobile, $message);
                    // EMAIL
                    // Mail::raw($message, function ($mail) use ($borrower,$loan) {
                    //   $message = "Segera lakuKAN pelunasan tagihan anda . besok adalah jatuh tempo contoh";
                    //   $mail->from('info@agus.com');
                    //   $mail->to('test@gmail.com')
                    //        ->subject($loan->maturity_date );
                    //   });
                }
               
            }
        }
        
    }

    public static function __Send_Short_Message($phone, $message){

        $curl = curl_init();

         curl_setopt_array($curl, array(
           CURLOPT_URL => "https://api.cankul.com",
           CURLOPT_RETURNTRANSFER => true,
           CURLOPT_ENCODING => "",
           CURLOPT_MAXREDIRS => 10,
           CURLOPT_TIMEOUT => 30,
           CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
           CURLOPT_CUSTOMREQUEST => "POST",
           CURLOPT_POSTFIELDS => "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"keys\"\r\n\r\np3463y\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"phone_number\"\r\n\r\n".$phone."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"messages_text\"\r\n\r\n".$message."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--",
           CURLOPT_HTTPHEADER => array(
             "Postman-Token: e5ed312d-1b10-4291-905c-ef78c7751154",
             "cache-control: no-cache",
             "content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW"
           ),
         ));

         $response = curl_exec($curl);
         $err = curl_error($curl);

         curl_close($curl);

         if ($err) {
           return $err;
         } else {
           return json_decode($response);
         }
     }
}
