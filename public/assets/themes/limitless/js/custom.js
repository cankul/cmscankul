/**
 * Created by Tj on 7/24/2016.
 */
$(document).ready(function () {
    if (jQuery().wysihtml5) {
        $('.wysihtml5').wysihtml5({});
    }
    if (jQuery().datepicker) {
        $('.date-picker').datepicker({
            orientation: "left",
            autoclose: true,
            format: "yyyy-mm-dd"
        });
        //$('body').removeClass("modal-open"); // fix bug when inline picker is used in modal
    }
    $('.time-picker').datetimepicker({
        format: 'HH:mm'
    });
    if (jQuery().TouchSpin) {
        $(".touchspin").TouchSpin({
            buttondown_class: 'btn blue',
            buttonup_class: 'btn blue',
            min: 0,
            max: 10000000000,
            step: 0.01,
            decimals: 2,
            boostat: 5,
            maxboostedstep: 1,
            prefix: ''
        });
    }
    $('[data-toggle="confirmation"]').confirmation({
        popout: true
    });
    $('[data-toggle="tooltip"]').tooltip();
    if (jQuery().select2) {
        $(".select2").select2({
            theme: "bootstrap"
        });
    }
    $(".fancybox").fancybox();
    $('.delete').on('click', function (e) {
        e.preventDefault();
        var href = $(this).attr('href');
        swal({
            title: 'Are you sure?',
            text: '',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ok',
            cancelButtonText: 'Cancel'
        }).then(function () {
            window.location = href;
        })
    });

        $('.decline').on('click', function (e) {
        e.preventDefault();
        var href = $(this).attr('href');



        swal({
          title: 'Reason For Decline',
          // input: 'checkbox',
          // // inputOptions: inputOptions,
          // inputPlaceholder: 'I have a bike',
           html:'<h6 align="left"><input type="checkbox" id="d1" placeholder="test"  /> Insuficient credit score </h6><p/>' +
                '<h6 align="left"><input type="checkbox" id="d2"  /> Mobile number cannot be verifide </h6><p/>' + 
                '<h6 align="left"><input type="checkbox" id="d3"  /> Address cannot be verifide </h6><p/>' +
                '<h6 align="left"> <input type="checkbox" id="d4"  /> Emergency contact cannot be verifide</h6><p/>' + 
                '<h6 align="left"> <input type="checkbox" id="d5"  /> have not complated a minimum of 80% installments on a previous loan</h6>',
          confirmButtonText: 'Confirmation',
          // textAlign: "left",
           preConfirm: function () {
            return new Promise(function (resolve) {
              resolve([
                    "d1="+ document.querySelector('#d1').checked,
                    "d2="+ document.querySelector('#d2').checked,
                    "d3="+ document.querySelector('#d3').checked,
                    "d4="+ document.querySelector('#d4').checked,
                    "d5="+ document.querySelector('#d5').checked
              ])
            })
          },
          onOpen: function () {
            $('#d1').focus()
          }
         
        }).then((result) => {
          // console.log(result);
          // console.log("result");
          //  console.log(href+"?"+result[0]+"&"+result[1]+"&"+result[2]+"&"+result[3]+"&"+result[4]);
          // swal(JSON.stringify(result));
          window.location =href+"?"+result[0]+"&"+result[1]+"&"+result[2]+"&"+result[3]+"&"+result[4];
        });
        
    });

    $('.blacklist').on('click', function (e) {
        e.preventDefault();
        var href = $(this).attr('href');
        // swal({
        //     title: 'Reason For blacklist',
        //         input: 'select',
        //         inputOptions: {
        //           'SRB': 'Serbia',
        //           'UKR': 'Ukraine',
        //           'HRV': 'Croatia'
        //         },
        //         inputValue: 'HRV',
        //         showCancelButton: true
        // }).then(function () {
        //     window.location = href;
        // })

        swal({
          title: 'Reason For Blacklist',
          
           html:'<h6 align="left"><input type="checkbox" id="b1" placeholder="test"  /> The borrower photos does not  match the ID card photo </h6><p/>' +
                '<h6 align="left"><input type="checkbox" id="b2"  /> Overdue installments of more than twice </h6><p/>' + 
                '<h6 align="left"><input type="checkbox" id="b3"  /> Infalid ID card </h6>', 
               
          confirmButtonText: 'Confirmation',
          // textAlign: "left",
           preConfirm: function () {
            return new Promise(function (resolve) {
              resolve([
                    "b1="+ document.querySelector('#b1').checked,
                    "b2="+ document.querySelector('#b2').checked,
                    "b3="+ document.querySelector('#b3').checked
              ])
            })
          },
          onOpen: function () {
            $('b1').focus()
          }
         
        }).then((result) => {
          // console.log(result);
          // console.log("result");
          //  console.log(href+"?"+result[0]+"&"+result[1]+"&"+result[2]);
          // swal(JSON.stringify(result))
           window.location =href+"?"+result[0]+"&"+result[1]+"&"+result[2];
        });
    });
   

    tinyMCE.init({
        selector: ".tinymce",
        theme: "modern",
        link_list: [
            {title: 'My page 1', value: 'http://www.tinymce.com'},
            {title: 'My page 2', value: 'http://www.tecrail.com'}
        ],
        plugins: [
            "advlist autolink link image lists charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking spellchecker",
            "table contextmenu directionality emoticons paste textcolor code "
        ],
        relative_urls: false,
        remove_script_host: false,
        convert_urls: true,
        browser_spellcheck: true,
        image_advtab: true,
        toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect",
        toolbar2: "image | link unlink anchor | print preview code  | youtube | qrcode | flickr | picasa | forecolor backcolor | easyColorPicker"
    });
    $(".numeric").numeric();
    $(".positive").numeric({negative: false});
    $(".positive-integer").numeric({decimal: false, negative: false});
    $(".decimal-2-places").numeric({decimalPlaces: 2});
    $(".decimal-4-places").numeric({decimalPlaces: 4});
    $('.styled').uniform({
        radioClass: 'choice',
    });
    $(".file-styled").uniform({
        fileButtonClass: 'action btn btn-primary'
    });
    $.extend($.fn.dataTable.defaults, {

        dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
        drawCallback: function () {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
            $('.delete').on('click', function (e) {
                e.preventDefault();
                var href = $(this).attr('href');
                swal({
                    title: 'Are youcc sure?',
                    text: '',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Ok',
                    cancelButtonText: 'Cancel'
                }).then(function () {
                    window.location = href;
                })
            });
              $('#mali').on('click', function (e) {
                e.preventDefault();
                var href = $(this).attr('href');
                swal({
                    title: 'Are you sure mali?',
                    text: '',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Ok',
                    cancelButtonText: 'Cancel'
                }).then(function () {
                    window.location = href;
                })
            });
        },
        preDrawCallback: function () {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
        }
    });
    $('.basic-datable').DataTable();
});
function isDecimalKey(el, evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    var number = el.value.split('.');
    if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 46 &&
        ((charCode != 65 && charCode != 86 && charCode != 67 && charCode != 99 && charCode != 120 && charCode != 118 && charCode != 97))) {
        alert("Only numbers or decimals are allowed");
        return false;
    }
    //1 decimal allowed
    if (number.length > 1 && charCode == 46) {
        return false;
    }

    var caratPos = getSelectionStart(el);
    var dotPos = el.value.indexOf(".");
    if (caratPos > dotPos && dotPos > -1 && (number[1].length > 1) && (charCode > 31)) {
        return false;
    }
    return true;
}
function isInterestKey(el, evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    var number = el.value.split('.');
    if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 46) {
        alert("Only numbers or decimals are allowed");
        return false;
    }
    //1 decimal allowed
    if (number.length > 1 && charCode == 46) {
        return false;
    }

    var caratPos = getSelectionStart(el);
    var dotPos = el.value.indexOf(".");
    if (caratPos > dotPos && dotPos > -1 && (number[1].length > 3) && (charCode > 31)) {
        return false;
    }
    return true;
}

function getSelectionStart(o) {
    if (o.createTextRange) {
        var r = document.selection.createRange().duplicate()
        r.moveEnd('character', o.value.length)
        if (r.text == '') return o.value.length
        return o.value.lastIndexOf(r.text)
    } else return o.selectionStart
}
function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57) && (charCode != 65 && charCode != 86 && charCode != 67 && charCode != 99 && charCode != 120 && charCode != 118 && charCode != 97)) {
        alert("Only numbers are allowed");
        return false;
    }
    return true;
}
