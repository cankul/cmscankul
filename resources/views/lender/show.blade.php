@extends('layouts.master')
@section('title')
    Lender
    {{-- {{trans_choice('general.borrower',1)}}  --}}
    {{trans_choice('general.detail',2)}}
@endsection
@section('content')
    @if($lender->blacklisted==1)
        <div class="row">
            <div class="col-sm-12">
                <div class="alert bg-danger">
                    <button type="button" class="close" data-dismiss="alert"><span>×</span><span
                                class="sr-only">Close</span></button>
                    {{trans_choice('general.lender_blacklist_notification',1)}}
                </div>
            </div>

        </div>
    @endif
    <!-- Detached sidebar -->

    <div class="sidebar-detached">
        <div class="sidebar sidebar-default sidebar-separate">
            <div class="sidebar-content">
                <!-- User details -->
                <div class="content-group">
                    {{-- conte --}}
                    <div class="panel-body bg-indigo-400 border-radius-top text-center"
                         style="background-image: url(http://demo.interface.club/limitless/assets/images/bg.png); background-size: contain;">
                        <div class="content-group-sm">
                            <h6 class="text-semibold no-margin-bottom">
                                {{$lender->title}} {{$lender->first_name}} {{$lender->last_name}}
                            </h6>
                            <span class="display-block">{{$lender->unique_number}}</span>
                            <?php 
                            $date1 = new DateTime($lender->dob);
                            $date2 = new DateTime();
                            $date=date_diff($date1,$date2);
                   
                            $years= $date1->diff($date2)->y;
                            $month = $date1->diff($date2)->m;
                            $days = $date1->diff($date2)->d;
                            $houre = $date1->diff($date2)->h;
                            
                            ?>

                            @if($lender->gender=="Male")
                                <span class="display-block">{{trans_choice('general.male',1)}}
                                   , {{$years}} Years {{$month}} Month {{$days}} Days
                                </span>
                            @endif
                            @if($lender->gender=="Female")
                                <span class="display-block">{{trans_choice('general.female',1)}}
                                   , {{$years}} Years {{$month}} Month {{$days}} Days
                                </span>
                            @endif
                        </div>

                        <a href="#" class="display-inline-block content-group-sm">
                            @if(!empty($lender->photo))
                                <a href="{{asset('uploads/'.$lender->photo)}}"
                                   class="display-inline-block content-group-sm fancybox"> 
                                   <img class="img-circle img-responsive"  src="{{asset('uploads/'.$lender->photo)}}"
                                            alt="user image" style="max-height: 120px!important;"/>
                                </a>
                            @else
                                <a href="#" class="display-inline-block content-group-sm "> 
                                    <img class="img-circle img-responsive"
                                         src="{{asset('assets/dist/img/user.png')}}"
                                         alt="user image" style="height: 120px!important;"/>
                                </a>
                            @endif
                        </a>

                        <ul class="list-inline list-inline-condensed no-margin-bottom">
                            <li><a href="{{url('communication/email/create?borrower_id='.$lender->id)}}"
                                   class="btn bg-indigo btn-rounded btn-icon" data-toggle="tooltip"
                                   title="{{trans_choice('general.email',1)}}"><i class="icon-envelop3"></i></a>
                            </li>
                            <li><a href="{{url('communication/sms/create?borrower_id='.$lender->id)}}"
                                   class="btn bg-indigo btn-rounded btn-icon" data-toggle="tooltip"
                                   title="{{trans_choice('general.sms',1)}}"><i class="icon-mobile"></i></a>
                            </li>
                            <li><a href="{{url('lender/'.$lender->id.'/edit')}}"
                                   class="btn bg-indigo btn-rounded btn-icon" data-toggle="tooltip"
                                   title="{{trans_choice('general.edit',1)}}"><i class=" icon-pen6"></i></a>
                            </li>
                        </ul>
                    </div>

                    <div class="panel no-border-top no-border-radius-top">
                        <ul class="navigation">
                            <li class="navigation-header">Navigation</li>
                            <li class="active"><a href="#profile" data-toggle="tab"><i class="icon-profile"></i>
                                    {{trans_choice('general.profile',1)}}</a></li>
                            <li><a href="#loans" data-toggle="tab"><i
                                            class="icon-balance"></i> {{trans_choice('general.deposit',2)}}</a></li>
                            <li><a href="#payments" data-toggle="tab"><i
                                            class="icon-coin-dollar"></i> {{trans_choice('general.payment',2)}} </a>
                            </li>
                        <li><a href="#savings" data-toggle="tab"><i
                                            class="icon-database2"></i> {{trans_choice('general.saving',2)}}</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- /user details -->
            </div>
        </div>
    </div>
    <!-- /detached sidebar -->
    <div class="container-detached">
        <div class="content-detached">
            <!-- Tab content -->
            <div class="tab-content">
                <div class="tab-pane fade in active" id="profile">
                    <div class="panel panel-white">
                        <div class="panel-heading">
                            <h6 class="panel-title">{{trans_choice('general.profile',1)}}</h6>
                            <div class="heading-elements">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-info dropdown-toggle margin"
                                            data-toggle="dropdown">
                                        {{trans_choice('general.lender',1)}} {{trans_choice('general.statement',1)}}
                                        <span class="fa fa-caret-down"></span></button>
                                    <ul class="dropdown-menu" role="menu">
                                        <li>
                                            <a href="{{url('loan/'.$lender->id.'/borrower_statement/print')}}"
                                               target="_blank"><i
                                                        class="icon-printer"></i> {{trans_choice('general.print',1)}} {{trans_choice('general.statement',1)}}
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{url('loan/'.$lender->id.'/borrower_statement/pdf')}}"
                                               target="_blank"><i
                                                        class="icon-file-pdf"></i> {{trans_choice('general.download',1)}} {{trans_choice('general.in',1)}} {{trans_choice('general.pdf',1)}}
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{url('loan/'.$lender->id.'/borrower_statement/email')}}"><i
                                                        class="icon-envelop"></i> {{trans_choice('general.email',1)}}
                                                {{trans_choice('general.statement',1)}}</a></li>
                                    <!--<li>
                                    <a href="{{url('loan/'.$lender->id.'/borrower_statement/excel')}}"
                                       target="_blank">{{trans_choice('general.download',1)}} {{trans_choice('general.in',1)}} {{trans_choice('general.excel',1)}}</a></li>

                                <li>
                                    <a href="{{url('loan/'.$lender->id.'/borrower_statement/csv')}}"
                                       target="_blank">{{trans_choice('general.download',1)}} {{trans_choice('general.in',1)}} {{trans_choice('general.csv',1)}}</a></li>-->
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <h6><b>{{trans_choice('general.basic',1)}} {{trans_choice('general.detail',2)}}</b>
                                    </h6>
                                    <table class="table table-striped table-hover">
                                        <tr>
                                            <td><b>{{trans_choice('general.business',1)}}</b></td>
                                            <td>{{$lender->business_name}}</td>
                                        </tr>
                                        <tr>
                                            <td><b>{{trans_choice('general.working_status',1)}}</b></td>
                                            <td>{{$lender->working_status}}</td>
                                        </tr>
                                        <tr>
                                            <td><b>{{trans_choice('general.dob',1)}}</b></td>
                                            <td>{{$lender->dob}}</td>
                                        </tr>
                                        <tr>
                                            <td><b>{{trans_choice('general.address',1)}}</b></td>
                                            <td>{{$lender->address}}</td>
                                        </tr>
                                        <tr>
                                            <td><b>{{trans_choice('general.city',1)}}</b></td>
                                            <td>{{$lender->city}}</td>
                                        </tr>
                                        <tr>
                                            <td><b>{{trans_choice('general.state',1)}}</b></td>
                                            <td>{{$lender->state}}</td>
                                        </tr>
                                        <tr>
                                            <td><b>{{trans_choice('general.zip',1)}}</b></td>
                                            <td>{{$lender->zip}}</td>
                                        </tr>
                                        <tr>
                                            <td><b>{{trans_choice('general.country',1)}}</b></td>
                                            <td>
                                                @if($lender->country)
                                                    {{$lender->country->name}}
                                                @endif
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="col-md-6">
                                    <h6>
                                        <b>{{trans_choice('general.contact',1)}} {{trans_choice('general.detail',2)}}</b>
                                    </h6>
                                    <table class="table table-striped table-hover">
                                        <tr>
                                            {{-- <td><b>{{trans_choice('general.phone',1)}}</b></td> --}}
                                            <td><b>Emergency Contact</b></td>
                                            
                                            @if(Sentinel::hasAnyAccess('EmergencyNumber.View'))
                                            <td>{{$lender->phone}}</td>
                                            @endif 
                                        </tr>
                                        <tr>
                                            <td><b>{{trans_choice('general.email',1)}}</b></td>
                                            <td>
                                                <a
                                                        href="{{url('communication/email/create?borrower_id='.$lender->id)}}">
                                                    
                                                    {{$lender->email}}
                                                   
                                                </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><b>{{trans_choice('general.mobile',1)}}</b></td>
                                            <td>
                                                <a
                                                        href="{{url('communication/sms/create?borrower_id='.$lender->id)}}">
                                                    @if(Sentinel::hasAccess('MobileNumber.View'))
                                                    {{$lender->mobile}}
                                                    @endif 
                                                </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            
                                            <td><b>Note Decline</b></td>
                                            <td>
                                                @if ($lender->blacklisted == 0)
                                                    @if ($lender->active == 0)
                                                        @if (!empty($note))
                                                            @for ($i = 0; $i < count($note); $i++)
                                                                {{ $i+1 }} .
                                                                {{ $note[$i][0]->description }}<br>
                                                            @endfor
                                                        @endif
                                                    @endif
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><b>Note Blacklist</b></td>
                                            <td>
                                                @if ($lender->blacklisted == 1)
                                                    @if (!empty($note_b))
                                                        @for ($i = 0; $i < count($note_b); $i++)
                                                        {{ $i+1 }} .
                                                        {{ $note_b[$i][0]->description }}<br>
                                                        @endfor
                                                    @endif
                                                @endif   
                                                
                                            </td>
                                        </tr>
                                    </table>
                                    <h6>
                                        <b>{{trans_choice('general.file',2)}}</b>
                                    </h6>
                                    <a data-toggle="collapse" data-parent="#accordion" href="#viewFiles">
                                        {{trans_choice('general.view',1)}} {{trans_choice('general.borrower',1)}} {{trans_choice('general.file',2)}}
                                    </a>

                                    <div id="viewFiles" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <ul class="list-unstyled">
                                            @if (!empty($files))
                                                @for ($i = 0; $i < count($files); $i++)
                                                    
                                                    <li>
                                                        <a href="https://aa.cankul.com/api.cangkul.com/storage/images/{{ $files[$i]->image_name }}"
                                                           target="_blank">{{ $files[$i]->image_name }}</a>
                                                           <br>
                                                           {{ $files[$i]->upload_date }}

                                                           
                                                    </li>
                                                @endfor
                                            @endif
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <h6><b>{{trans_choice('general.custom_field',2)}}</b></h6>
                                    <table class="table table-striped table-hover">
                                        @foreach($custom_fields as $key)
                                            <tr>
                                                <td>
                                                    @if(!empty($key->custom_field))
                                                        <strong>{{$key->custom_field->name}}:</strong>
                                                    @endif
                                                </td>
                                                <td>{{$key->name}}</td>
                                            </tr>
                                        @endforeach
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade in " id="loans">
                    <div class="panel panel-white">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{trans_choice('general.deposit',2)}}</h3>

                            <div class="heading-elements">

                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                
                                <table id="repayments-data-table"
                                class="table  table-condensed table-hover">
                             <thead>
                             <tr>
                                 <th>
                                     {{trans_choice('general.id',1)}}
                                 </th>
                                 <th>
                                     {{trans_choice('general.date',1)}}
                                 </th>
                                 <th>
                                     {{trans_choice('general.submitted',1)}} {{trans_choice('general.on',1)}}
                                 </th>
                                 <th>
                                     {{trans_choice('general.type',1)}}
                                 </th>

                                 <th>
                                     {{trans_choice('general.debit',1)}}
                                 </th>
                                 <th>
                                     {{trans_choice('general.credit',1)}}
                                 </th>
                                 <th>
                                     {{trans_choice('general.balance',1)}}
                                 </th>
                                 <th>
                                     {{trans_choice('general.detail',2)}}
                                 </th>

                             </tr>
                             </thead>
                             <tbody>
                             <?php
                             $balance = 0;
                             ?>
                             @foreach( $SavingTransaction as $key)
                                 <?php
                                 $balance = $balance + ($key->credit - $key->debit);
                                 ?>
                                 <tr>
                                     <td>{{$key->id}}</td>
                                     <td>{{$key->date}} {{$key->time}}</td>
                                     <td>{{$key->created_at}}</td>
                                     <td>
                                         @if($key->type=='deposit')
                                             {{trans_choice('general.deposit',1)}}
                                         @endif
                                         @if($key->type=='withdrawal')
                                             {{trans_choice('general.withdrawal',1)}}
                                         @endif
                                         @if($key->type=='bank_fees')
                                             {{trans_choice('general.charge',1)}}
                                         @endif
                                         @if($key->type=='interest')
                                             {{trans_choice('general.interest',1)}}
                                         @endif
                                         @if($key->type=='dividend')
                                             {{trans_choice('general.dividend',1)}}
                                         @endif
                                         @if($key->type=='transfer')
                                             {{trans_choice('general.transfer',1)}}
                                         @endif
                                         @if($key->type=='transfer_fund')
                                             {{trans_choice('general.transfer',1)}}
                                         @endif
                                         @if($key->type=='transfer_loan')
                                             {{trans_choice('general.transfer',1)}}
                                         @endif
                                         @if($key->type=='guarantee')
                                             {{trans_choice('general.on',1)}} {{trans_choice('general.hold',1)}}
                                         @endif
                                         @if($key->reversed==1)
                                             @if($key->reversal_type=="user")
                                                 <span class="text-danger"><b>({{trans_choice('general.user',1)}} {{trans_choice('general.reversed',1)}}
                                                         )</b></span>
                                             @endif
                                             @if($key->reversal_type=="system")
                                                 <span class="text-danger"><b>({{trans_choice('general.system',1)}} {{trans_choice('general.reversed',1)}}
                                                         )</b></span>
                                             @endif
                                         @endif
                                     </td>
                                     <td>{{number_format($key->debit,2)}}</td>
                                     <td>{{number_format($key->credit,2)}}</td>
                                     <td>{{number_format($balance,2)}}</td>
                                     <td>{{$key->receipt}}</td>

                                 </tr>
                             @endforeach
                             </tbody>
                         </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade in " id="payments">
                    <div class="panel panel-white">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{trans_choice('general.repayment',2)}}</h3>
                            <div class="heading-elements">

                            </div>
                        </div>
                        <div class="panel-body table-responsive">
                            <table id="repayments-data-table"
                                   class="table  table-condensed table-hover">
                                <thead>
                                <tr>
                                    <th>
                                        {{trans_choice('general.collection',1)}} {{trans_choice('general.date',1)}}
                                    </th>
                                    <th>
                                        {{trans_choice('general.collected_by',1)}}
                                    </th>
                                    <th>
                                        {{trans_choice('general.method',1)}}
                                    </th>
                                    <th>
                                        {{trans_choice('general.amount',1)}}
                                    </th>
                                    <th>
                                        {{trans_choice('general.action',1)}}
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach(\App\Models\LoanTransaction::where('borrower_id',$lender->id)->where('transaction_type','repayment')->where('reversed',0)->get() as $key)
                                    <tr>
                                        <td>{{$key->date}}</td>
                                        <td>
                                            @if(!empty($key->user))
                                                {{$key->user->first_name}} {{$key->user->last_name}}
                                            @endif
                                        </td>
                                        <td>
                                            @if(!empty($key->loan_repayment_method))
                                                {{$key->loan_repayment_method->name}}
                                            @endif
                                        </td>
                                        <td>{{number_format($key->credit,2)}}</td>
                                        <td class="text-center">
                                            <ul class="icons-list">
                                                <li class="dropdown">
                                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                        <i class="icon-menu9"></i>
                                                    </a>
                                                    <ul class="dropdown-menu dropdown-menu-right">
                                                        <li>
                                                            <a href="{{url('loan/transaction/'.$key->id.'/show')}}"><i
                                                                        class="fa fa-search"></i> {{ trans_choice('general.view',1) }}
                                                            </a></li>
                                                        <li>
                                                        @if($key->transaction_type=='repayment' && $key->reversible==1)
                                                            <li>
                                                                <a href="{{url('loan/transaction/'.$key->id.'/print')}}"
                                                                   target="_blank"><i
                                                                            class="icon-printer"></i> {{ trans_choice('general.print',1) }} {{trans_choice('general.receipt',1)}}
                                                                </a></li>
                                                            <li>
                                                                <a href="{{url('loan/transaction/'.$key->id.'/pdf')}}"
                                                                   target="_blank"><i
                                                                            class="icon-file-pdf"></i> {{ trans_choice('general.pdf',1) }} {{trans_choice('general.receipt',1)}}
                                                                </a></li>
                                                            <li>
                                                                <a href="{{url('loan/repayment/'.$key->id.'/edit')}}"><i
                                                                            class="fa fa-edit"></i> {{ trans('general.edit') }}
                                                                </a></li>
                                                            <li>
                                                                <a href="{{url('loan/repayment/'.$key->id.'/reverse')}}"
                                                                   class="delete"><i
                                                                            class="fa fa-minus-circle"></i> {{ trans('general.reverse') }}
                                                                </a></li>
                                                        @endif
                                                    </ul>
                                                </li>
                                            </ul>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade in " id="savings">
                    <div class="panel panel-white">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{trans_choice('general.deposit',2)}}</h3>

                            <div class="heading-elements">

                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                
                                <table id="repayments-data-table"
                                class="table  table-condensed table-hover">
                             <thead>
                             <tr>
                                 <th>
                                     {{trans_choice('general.id',1)}}
                                 </th>
                                 <th>
                                     {{trans_choice('general.date',1)}}
                                 </th>
                                 <th>
                                     {{trans_choice('general.submitted',1)}} {{trans_choice('general.on',1)}}
                                 </th>
                                 <th>
                                     {{trans_choice('general.type',1)}}
                                 </th>

                                 <th>
                                     {{trans_choice('general.debit',1)}}
                                 </th>
                                 <th>
                                     {{trans_choice('general.credit',1)}}
                                 </th>
                                 <th>
                                     {{trans_choice('general.balance',1)}}
                                 </th>
                                 <th>
                                     {{trans_choice('general.detail',2)}}
                                 </th>

                             </tr>
                             </thead>
                             <tbody>
                             <?php
                             $balance = 0;
                             ?>
                             @foreach( $saving as $key)
                                 <?php
                                 $balance = $balance + ($key->credit - $key->debit);
                                 ?>
                                 <tr>
                                     <td>{{$key->id}}</td>
                                     <td>{{$key->date}} {{$key->time}}</td>
                                     <td>{{$key->created_at}}</td>
                                     <td>
                                         @if($key->type=='deposit')
                                             {{trans_choice('general.deposit',1)}}
                                         @endif
                                         @if($key->type=='withdrawal')
                                             {{trans_choice('general.withdrawal',1)}}
                                         @endif
                                         @if($key->type=='bank_fees')
                                             {{trans_choice('general.charge',1)}}
                                         @endif
                                         @if($key->type=='interest')
                                             {{trans_choice('general.interest',1)}}
                                         @endif
                                         @if($key->type=='dividend')
                                             {{trans_choice('general.dividend',1)}}
                                         @endif
                                         @if($key->type=='transfer')
                                             {{trans_choice('general.transfer',1)}}
                                         @endif
                                         @if($key->type=='transfer_fund')
                                             {{trans_choice('general.transfer',1)}}
                                         @endif
                                         @if($key->type=='transfer_loan')
                                             {{trans_choice('general.transfer',1)}}
                                         @endif
                                         @if($key->type=='guarantee')
                                             {{trans_choice('general.on',1)}} {{trans_choice('general.hold',1)}}
                                         @endif
                                         @if($key->reversed==1)
                                             @if($key->reversal_type=="user")
                                                 <span class="text-danger"><b>({{trans_choice('general.user',1)}} {{trans_choice('general.reversed',1)}}
                                                         )</b></span>
                                             @endif
                                             @if($key->reversal_type=="system")
                                                 <span class="text-danger"><b>({{trans_choice('general.system',1)}} {{trans_choice('general.reversed',1)}}
                                                         )</b></span>
                                             @endif
                                         @endif
                                     </td>
                                     <td>{{number_format($key->debit,2)}}</td>
                                     <td>{{number_format($key->credit,2)}}</td>
                                     <td>{{number_format($balance,2)}}</td>
                                     <td>{{$key->receipt}}</td>

                                 </tr>
                             @endforeach
                             </tbody>
                         </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>



@endsection
@section('footer-scripts')

    <script>
        $('#loan-data-table').DataTable({
            dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
            autoWidth: false,
            columnDefs: [{
                orderable: false,
                width: '100px',
                targets: [8]
            }]
            , "order": [[0, "desc"]],
            language: {
                "lengthMenu": "{{ trans('general.lengthMenu') }}",
                "zeroRecords": "{{ trans('general.zeroRecords') }}",
                "info": "{{ trans('general.info') }}",
                "infoEmpty": "{{ trans('general.infoEmpty') }}",
                "search": "{{ trans('general.search') }}:",
                "infoFiltered": "{{ trans('general.infoFiltered') }}",
                "paginate": {
                    "first": "{{ trans('general.first') }}",
                    "last": "{{ trans('general.last') }}",
                    "next": "{{ trans('general.next') }}",
                    "previous": "{{ trans('general.previous') }}"
                }
            }
        });
        $('#repayments-data-table').DataTable({
            dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
            autoWidth: false,
            columnDefs: [{
                orderable: false,
                width: '100px',
                targets: [4]
            }],
            "order": [[0, "desc"]],
            language: {
                "lengthMenu": "{{ trans('general.lengthMenu') }}",
                "zeroRecords": "{{ trans('general.zeroRecords') }}",
                "info": "{{ trans('general.info') }}",
                "infoEmpty": "{{ trans('general.infoEmpty') }}",
                "search": "{{ trans('general.search') }}:",
                "infoFiltered": "{{ trans('general.infoFiltered') }}",
                "paginate": {
                    "first": "{{ trans('general.first') }}",
                    "last": "{{ trans('general.last') }}",
                    "next": "{{ trans('general.next') }}",
                    "previous": "{{ trans('general.previous') }}"
                }
            },
        });

    </script>
    <script>
        $(document).ready(function () {
            $('body').addClass('has-detached-left');
            $('.deletePayment').on('click', function (e) {
                e.preventDefault();
                var href = $(this).attr('href');
                swal({
                    title: '{{trans_choice('general.are_you_sure',1)}}',
                    text: 'If you delete a payment, a fully paid loan may change status to open.',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: '{{trans_choice('general.ok',1)}}',
                    cancelButtonText: '{{trans_choice('general.cancel',1)}}'
                }).then(function () {
                    window.location = href;
                })
            });
        });
    </script>
@endsection