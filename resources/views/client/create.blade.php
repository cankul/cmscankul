@extends('client.layout')
@section('title')
    Lender
@endsection
@section('content')
    <div class="panel panel-white">
        <div class="panel-heading">
            <h6 class="panel-title">{{trans_choice('general.add',1)}} Lender</h6>

            <div class="heading-elements">
            </div>
        </div>
        {!! Form::open(array('url' => url('client/saving/storeSavings/'), 'method' => 'post', 'id' => 'savings_form',"enctype"=>"multipart/form-data", 'class' => 'form-horizontal')) !!}
        <div class="panel-body">
           
            <div class="form-group">
                {!! Form::label('savings_product_id',trans_choice('general.product',1)." *",array('class'=>'col-sm-3 control-label')) !!}
                <div class="col-sm-5">
                    {!! Form::select('savings_product_id',$savings_products,null,array('class' => 'select2 form-control','required'=>'','id'=>'borower_id')) !!}
                </div>
                <div class="col-sm-4">
                    <i class="icon-info3" data-toggle="tooltip" title="Select the Savings product"></i>
                </div>
            </div>
            <div class="form-group">
                {!! Form::label('date',trans_choice('general.date',1)." *",array('class'=>'col-sm-3 control-label')) !!}
                <div class="col-sm-5">
                    {!! Form::text('date',date("Y-m-d"), array('class' => 'form-control date-picker', 'placeholder'=>"yyyy-mm-dd",'required'=>'required','id'=>'date')) !!}
                </div>
            </div>
            <div class="form-group">
                {!! Form::label('notes',trans_choice('general.note',2),array('class'=>'col-sm-3 control-label')) !!}
                <div class="col-sm-5">
                    {!! Form::textarea('notes',null, array('class' => 'form-control', 'rows'=>'2',)) !!}
                    <input type="hidden" name="borrower_id" value="{{ $Lender->id }}">
                </div>
            </div>
            {{-- <p class="bg-navy color-palette">{{trans_choice('general.charge',2)}}</p> --}}

          <!--  -->
        </div>
        <!-- /.panel-body -->
        <div class="panel-footer">
            <div class="heading-elements">
                <button type="submit" class="btn btn-primary pull-right">{{trans_choice('general.save',1)}}</button>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
    <!-- /.box -->
@endsection

@section('footer-scripts')
    <script>
        $(document).ready(function () {

            $('#savings_product_id').change(function (e) {
                window.location = "{!! url('saving/create?product_id=') !!}" + $("#savings_product_id").val()+"&borrower_id="+ $("#borrower_id").val();
            })

        });
    </script>
    <script>
        $("#savings_form").validate({
            rules: {
                field: {
                    required: true,
                    number: true
                }
            }
        });
    </script>
@endsection