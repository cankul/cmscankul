@extends('client.layout')
@section('title')
    {{ trans('general.dashboard') }}
@endsection

@section('content')


     <div class="panel panel-white">
        <div class="panel-body">
            <div class="row">
                <div class="col-sm-3 col-xs-6">
                    <div class="description-block border-right">
                        @if(\App\Models\Setting::where('setting_key', 'currency_position')->first()->setting_value=='left')
                            <h5 class="description-header"> {{ \App\Models\Setting::where('setting_key', 'currency_symbol')->first()->setting_value }} {{ number_format(\App\Helpers\GeneralHelper::lender_total_deposit($Lender->id),2) }} </h5>
                        @else
                            <h5 class="description-header"> {{ number_format(\App\Helpers\GeneralHelper::lender_total_deposit($Lender->id),2) }}   {{ \App\Models\Setting::where('setting_key', 'currency_symbol')->first()->setting_value}}</h5>
                        @endif
                        <span class="description-text">Deposit</span>
                    </div>
                </div>
                <div class="col-sm-3 col-xs-6">
                    <div class="description-block border-right">
                        @if(\App\Models\Setting::where('setting_key', 'currency_position')->first()->setting_value=='left')
                            <h5 class="description-header"> {{ \App\Models\Setting::where('setting_key', 'currency_symbol')->first()->setting_value }} {{number_format(\App\Helpers\GeneralHelper::lender_loan_releas($Lender->id),2)}} </h5>
                        @else
                            <h5 class="description-header">  {{number_format(\App\Helpers\GeneralHelper::lender_loan_releas($Lender->id),2)}} {{ \App\Models\Setting::where('setting_key', 'currency_symbol')->first()->setting_value}}</h5>
                        @endif
                        <span class="description-text">Loans Released</span>
                    </div>

                </div>

                <div class="col-sm-3 col-xs-6">
                    <div class="description-block border-right">
                        @if(\App\Models\Setting::where('setting_key', 'currency_position')->first()->setting_value=='left')
                        <h5 class="description-header"> {{ \App\Models\Setting::where('setting_key', 'currency_symbol')->first()->setting_value }} {{ number_format(\App\Helpers\GeneralHelper::lender_total_deposit($Lender->id) - \App\Helpers\GeneralHelper::lender_loan_releas($Lender->id),2) }} </h5>
                      
                             <!-- <h5 class="description-header">{{ \App\Models\Setting::where('setting_key', 'currency_symbol')->first()->setting_value}} {{number_format(\App\Helpers\GeneralHelper::lender_total_balance($Lender->id),2)}}  </h5> -->
                        @else
                            <h5 class="description-header"> {{number_format(\App\Helpers\GeneralHelper::lender_total_balance($Lender->id))}}  {{ \App\Models\Setting::where('setting_key', 'currency_symbol')->first()->setting_value}}</h5>
                        @endif
                        <span class="description-text">Balances</span>

                    </div>

                </div>

                <div class="col-sm-3 col-xs-6">
                    <div class="description-block">
                        @if(\App\Models\Setting::where('setting_key', 'currency_position')->first()->setting_value=='left')
                            <h5 class="description-header">{{ \App\Models\Setting::where('setting_key', 'currency_symbol')->first()->setting_value }}
                             {{number_format(\App\Helpers\GeneralHelper::lender_fees($Lender->id),2)}} 
                               </h5>
                        @else
                            
                        @endif
                        <span class="description-text">Fees</span>
                    </div>
                    
                </div>
            </div>

        </div>
    </div>
   <!--  <div class="row">

        <div class="col-md-7">

            <div class="panel panel-white">
                <div class="panel-heading">
                    <h6 class="panel-title">{{trans_choice('general.loan',2)}}</h6>

                    <div class="heading-elements">

                    </div>
                </div>
                <div class="panel-body table-responsive ">
                    <table id="loan-data-table" class="table  table-condensed">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>{{trans_choice('general.principal',1)}}</th>
                            <th>{{trans_choice('general.balance',1)}}</th>
                            <th>{{trans_choice('general.released',1)}}</th>
                            <th>{{trans_choice('general.status',1)}}</th>
                            <th>{{trans_choice('general.action',1)}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($Lender->loans as $key)
                            <tr>

                                <td>{{$key->id}}</td>
                                <td>{{number_format($key->principal,2)}}</td>
                                <td>{{number_format(\App\Helpers\GeneralHelper::loan_total_balance($key->id),2)}}</td>
                                <td>{{$key->release_date}}</td>


                                <td>
                                    @if($key->maturity_date<date("Y-m-d") && \App\Helpers\GeneralHelper::loan_total_balance($key->id)>0)
                                        <span class="label label-danger">{{trans_choice('general.past_maturity',1)}}</span>
                                    @else
                                        @if($key->status=='pending')
                                            <span class="label label-warning">{{trans_choice('general.pending',1)}} {{trans_choice('general.approval',1)}}</span>
                                        @endif
                                        @if($key->status=='approved')
                                            <span class="label label-info">{{trans_choice('general.awaiting',1)}} {{trans_choice('general.disbursement',1)}}</span>
                                        @endif
                                        @if($key->status=='disbursed')
                                            <span class="label label-info">{{trans_choice('general.active',1)}}</span>
                                        @endif
                                        @if($key->status=='declined')
                                            <span class="label label-danger">{{trans_choice('general.declined',1)}}</span>
                                        @endif
                                        @if($key->status=='withdrawn')
                                            <span class="label label-danger">{{trans_choice('general.withdrawn',1)}}</span>
                                        @endif
                                        @if($key->status=='written_off')
                                            <span class="label label-danger">{{trans_choice('general.written_off',1)}}</span>
                                        @endif
                                        @if($key->status=='closed')
                                            <span class="label label-success">{{trans_choice('general.closed',1)}}</span>
                                        @endif
                                        @if($key->status=='pending_reschedule')
                                            <span class="label label-warning">{{trans_choice('general.pending',1)}} {{trans_choice('general.reschedule',1)}}</span>
                                        @endif
                                        @if($key->status=='rescheduled')
                                            <span class="label label-info">{{trans_choice('general.rescheduled',1)}}</span>
                                        @endif
                                    @endif
                                </td>
                                <td>
                                    <a href="{{ url('client/loan/'.$key->id.'/show') }}"
                                       class="btn btn-info btn-xs legitRipple" data-toggle="tooltip"
                                       data-title="{{ trans_choice('general.detail',2) }}"><i
                                                class="fa fa-search"></i>
                                    </a>
                                    @if(\App\Models\Setting::where('setting_key','enable_online_payment')->first()->setting_value==1 && \App\Helpers\GeneralHelper::loan_total_balance($key->id)>0)
                                        <a href="{{ url('client/loan/'.$key->id.'/pay') }}"
                                           class="btn btn-success btn-xs legitRipple" data-toggle="tooltip"
                                           data-title="{{ trans('general.pay') }}"><i
                                                    class="fa fa-money"></i>
                                        </a>
                                    @endif
                                </td>

                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-5">
            <div class="panel panel-white">
                <div class="panel-heading">
                    <h6 class="panel-title">{{trans_choice('general.repayment',2)}}</h6>

                    <div class="heading-elements">

                    </div>
                </div>
                <div class="panel-body table-responsive">
                    <table id="view-repayments"
                           class="table table-condensed">
                        <thead>
                        <tr role="row">
                            <th>
                                {{trans_choice('general.collection',1)}} {{trans_choice('general.date',1)}}
                            </th>
                            <th>
                                {{trans_choice('general.method',1)}}
                            </th>
                            <th>
                                {{trans_choice('general.amount',1)}}
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach(\App\Models\LoanTransaction::where('transaction_type',
            'repayment')->where('reversed', 0)->where('borrower_id', $Lender->id)->get() as $key)


                            <tr>
                                <td>{{$key->date}}</td>
                                <td>
                                    @if(!empty($key->loan_repayment_method))
                                        {{$key->loan_repayment_method->name}}
                                    @endif
                                </td>
                                <td>{{number_format($key->credit,2)}}</td>

                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div> -->

    <div class="row">



        <div class="col-md-12">
            <div class="panel panel-white">
            <div class="panel-body ">
                    <h6 class="panel-title">{{trans_choice('Lender Products',2)}}</h6>
                    <hr>
                    <div class="heading-elements">

                    </div>
            <div class="table-responsive">
                <table id="data-table" class="table table-striped table-condensed table-hover">
                    <thead>
                    <tr >
                        <th>{{trans_choice('general.product',1)}}</th>
                        <th>{{trans_choice('general.interest_rate_per_annum',1)}}</th>
                        <th>{{trans_choice('general.interest_posting_frequency',1)}}</th>
                        <th>{{trans_choice('general.minimum',1)}} {{trans_choice('general.balance',1)}}</th>
                        {{-- <th>{{ trans_choice('general.action',1) }}</th> --}}
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($savings as $key)
                        <tr>
                            <td>{{ $key->name }}</td>
                            <td>{{ $key->interest_rate }}</td>
                            <td>
                                @if($key->interest_posting==1)
                                    {{trans_choice('general.every_1_month',1)}}
                                @endif
                                @if($key->interest_posting==2)
                                    {{trans_choice('general.every_2_month',1)}}
                                @endif
                                @if($key->interest_posting==3)
                                    {{trans_choice('general.every_3_month',1)}}
                                @endif
                                @if($key->interest_posting==4)
                                    {{trans_choice('general.every_4_month',1)}}
                                @endif
                                @if($key->interest_posting==5)
                                    {{trans_choice('general.every_6_month',1)}}
                                @endif
                                @if($key->interest_posting==6)
                                    {{trans_choice('general.every_12_month',1)}}

                                @endif
                            </td>
                            <td>{{ $key->minimum_balance }}</td>
                            {{-- <td>
                                <ul class="icons-list">
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                            <i class="icon-menu9"></i>
                                        </a>
                                        <ul class="dropdown-menu dropdown-menu-right">
                                           
                                                <li><a href="{{ url('saving/savings_product/'.$key->id.'/edit') }}"><i
                                                                class="fa fa-edit"></i> {{ trans('general.edit') }} </a>
                                                </li>
                                            <li><a href="{{ url('saving/savings_product/'.$key->id.'/delete') }}"
                                                       class="delete"><i
                                                                class="fa fa-trash"></i> {{ trans('general.delete') }}
                                                    </a>
                                                </li>
                                           
                                        </ul>
                                    </li>
                                </ul>
                            </td> --}}
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
            </div>
        </div>

    </div>
        <div class="panel panel-white">
        <div class="panel-heading">
            <h6 class="panel-title">{{trans_choice('Lender',2)}} {{trans_choice('general.transaction',2)}}</h6>

            <div class="heading-elements">

            </div>
        </div>
        <div class="panel-body ">
            <div class="table-responsive">
                <table id="repayments-data-table" class="table  table-condensed table-hover">
                    <thead>
                    <tr>
                        <th>
                            {{trans_choice('general.id',1)}}
                        </th>
                        <th>
                            {{trans_choice('general.date',1)}}
                        </th>
                        <th>
                            {{trans_choice('general.submitted',1)}} {{trans_choice('general.on',1)}}
                        </th>
                        <th>
                            {{trans_choice('general.type',1)}}
                        </th>

                        <th>
                            {{trans_choice('general.debit',1)}}
                        </th>
                        <th>
                            {{trans_choice('general.credit',1)}}
                        </th>
                        <th>
                            {{trans_choice('general.detail',2)}}
                        </th>
                        {{-- <th class="text-center">
                            {{trans_choice('general.action',1)}}
                        </th> --}}
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($data as $key)
                        <tr>
                            <td>{{$key->id}}</td>
                            <td>{{$key->date}} {{$key->time}}</td>
                            <td>{{$key->created_at}}</td>
                            <td>
                                @if($key->type=='deposit')
                                    {{trans_choice('general.deposit',1)}}
                                @endif
                                @if($key->type=='withdrawal')
                                    {{trans_choice('general.withdrawal',1)}}
                                @endif
                                @if($key->type=='bank_fees')
                                    {{trans_choice('general.charge',1)}}
                                @endif
                                @if($key->type=='interest')
                                    {{trans_choice('general.interest',1)}}
                                @endif
                                @if($key->type=='dividend')
                                    {{trans_choice('general.dividend',1)}}
                                @endif
                                @if($key->type=='transfer')
                                    {{trans_choice('general.transfer',1)}}
                                @endif
                                @if($key->type=='transfer_fund')
                                    {{trans_choice('general.transfer',1)}}
                                @endif
                                @if($key->type=='transfer_loan')
                                    {{trans_choice('general.transfer',1)}}
                                @endif
                                @if($key->type=='guarantee')
                                    {{trans_choice('general.on',1)}} {{trans_choice('general.hold',1)}}
                                @endif
                                @if($key->reversed==1)
                                    @if($key->reversal_type=="user")
                                        <span class="text-danger"><b>({{trans_choice('general.user',1)}} {{trans_choice('general.reversed',1)}}
                                                )</b></span>
                                    @endif
                                    @if($key->reversal_type=="system")
                                        <span class="text-danger"><b>({{trans_choice('general.system',1)}} {{trans_choice('general.reversed',1)}}
                                                )</b></span>
                                    @endif
                                @endif
                            </td>
                            <td>{{number_format($key->debit,2)}}</td>
                            <td>{{number_format($key->credit,2)}}</td>
                            <td>{{$key->receipt}}</td>
                            {{-- <td class="text-center">
                                <ul class="icons-list">
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle"
                                           data-toggle="dropdown">
                                            <i class="icon-menu9"></i>
                                        </a>
                                        <ul class="dropdown-menu dropdown-menu-right">
                                            @if($key->reversed==0)
                                                <li>
                                                    <a href="{{url('saving/savings_transaction/'.$key->id.'/show')}}"><i
                                                                class="fa fa-search"></i> {{ trans_choice('general.view',1) }}
                                                    </a></li>
                                                <li>
                                                    <a href="{{url('saving/savings_transaction/'.$key->id.'/print')}}"
                                                       target="_blank"><i
                                                                class="icon-printer"></i> {{ trans_choice('general.print',1) }} {{trans_choice('general.receipt',1)}}
                                                    </a></li>
                                                <li>
                                                    <a href="{{url('saving/savings_transaction/'.$key->id.'/pdf')}}"
                                                       target="_blank"><i
                                                                class="icon-file-pdf"></i> {{ trans_choice('general.pdf',1) }} {{trans_choice('general.receipt',1)}}
                                                    </a></li>
                                            @endif
                                            @if($key->reversed==0 && $key->reversible==1)

                                                <li>
                                                    <a href="{{url('saving/savings_transaction/'.$key->id.'/edit')}}"><i
                                                                class="fa fa-edit"></i> {{ trans('general.edit') }}
                                                    </a></li>
                                                <li>
                                                    <a href="{{url('saving/savings_transaction/'.$key->id.'/reverse')}}"
                                                       class="delete"><i
                                                                class="fa fa-minus-circle"></i> {{ trans('general.reverse') }}
                                                    </a></li>
                                            @endif
                                        </ul>
                                    </li>
                                </ul>
                            </td> --}}
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /.panel-body -->
    </div>
    <!-- /.box -->
    <script>
        $(document).ready(function () {

        });
    </script>
@endsection
@section('footer-scripts')
    <script>
        $('#repayments-data-table').DataTable({
            dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
            autoWidth: false,
            columnDefs: [{
                orderable: false,
                width: '100px',
                targets: [7]
            }],
            "order": [[1, "asc"]],
            language: {
                "lengthMenu": "{{ trans('general.lengthMenu') }}",
                "zeroRecords": "{{ trans('general.zeroRecords') }}",
                "info": "{{ trans('general.info') }}",
                "infoEmpty": "{{ trans('general.infoEmpty') }}",
                "search": "{{ trans('general.search') }}:",
                "infoFiltered": "{{ trans('general.infoFiltered') }}",
                "paginate": {
                    "first": "{{ trans('general.first') }}",
                    "last": "{{ trans('general.last') }}",
                    "next": "{{ trans('general.next') }}",
                    "previous": "{{ trans('general.previous') }}"
                }
            },
            drawCallback: function () {
                $('.delete').on('click', function (e) {
                    e.preventDefault();
                    var href = $(this).attr('href');
                    swal({
                        title: 'Are you sure?',
                        text: '',
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Ok',
                        cancelButtonText: 'Cancel'
                    }).then(function () {
                        window.location = href;
                    })
                });
            }
        });
    </script>
    <script>
        $('#loan-data-table').DataTable({
            "order": [[0, "asc"]],
            "columnDefs": [
                {"orderable": false, "targets": [5]}
            ],
            "language": {
                "lengthMenu": "{{ trans('general.lengthMenu') }}",
                "zeroRecords": "{{ trans('general.zeroRecords') }}",
                "info": "{{ trans('general.info') }}",
                "infoEmpty": "{{ trans('general.infoEmpty') }}",
                "search": "{{ trans('general.search') }}",
                "infoFiltered": "{{ trans('general.infoFiltered') }}",
                "paginate": {
                    "first": "{{ trans('general.first') }}",
                    "last": "{{ trans('general.last') }}",
                    "next": "{{ trans('general.next') }}",
                    "previous": "{{ trans('general.previous') }}"
                }
            },
            responsive: false
        });
        $('#view-repayments').DataTable({
            "order": [[0, "asc"]],
            "columnDefs": [
                {"orderable": false, "targets": []}
            ],
            "language": {
                "lengthMenu": "{{ trans('general.lengthMenu') }}",
                "zeroRecords": "{{ trans('general.zeroRecords') }}",
                "info": "{{ trans('general.info') }}",
                "infoEmpty": "{{ trans('general.infoEmpty') }}",
                "search": "{{ trans('general.search') }}",
                "infoFiltered": "{{ trans('general.infoFiltered') }}",
                "paginate": {
                    "first": "{{ trans('general.first') }}",
                    "last": "{{ trans('general.last') }}",
                    "next": "{{ trans('general.next') }}",
                    "previous": "{{ trans('general.previous') }}"
                }
            },
            responsive: false
        });
    </script>
@endsection