@extends('layouts.master')
@section('title'){{trans_choice('general.edit',1)}} {{ $disbursement[0]->title}}
@endsection
@section('content')
<div class="box">
    <div class="panel-heading">
        <h6 class="panel-title">{{trans_choice('general.edit',1)}}  {{ $disbursement[0]->title}}</h6>

        <div class="heading-elements">

        </div>
    </div>
    {!! Form::open(array('url' => url('bank/disb/sub/'.$disbursement_method[0]->id.'/update'), 'method' => 'post', 'class' => 'form-horizontal')) !!}
    <div class="panel-body">
        <div class="form-group">
            {!! Form::label('title',trans_choice('Name Bank',1),array('class'=>'col-sm-3 control-label')) !!}
            <div class="col-sm-5">
                {!! Form::text('title',$disbursement_method[0]->title, array('class' => 'form-control', 'placeholder'=>"",'required'=>'required')) !!}
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('step',trans_choice('Step',1),array('class'=>'col-sm-3 control-label')) !!}
            <div class="col-sm-9">
                {!! Form::textarea('step',$disbursement_method[0]->step, array('class' => 'form-control', 'rows'=>"4")) !!}
            </div>
        </div>

    </div>
    <!-- /.panel-body -->
    <div class="panel-footer">
        <button type="submit" class="btn btn-primary pull-right">{{trans_choice('general.save',1)}}</button>
    </div>
    {!! Form::close() !!}
</div>
<!-- /.box -->
@endsection

