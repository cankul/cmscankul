@extends('layouts.master')
@section('title'){{trans_choice('edit',1)}} {{trans_choice('Push',1)}} {{trans_choice('Notification',1)}}
@endsection
@section('content')
<div class="box">
    <div class="panel-heading">
        <h6 class="panel-title">{{trans_choice('general.edit',1)}} {{trans_choice('Push',1)}} {{trans_choice('Notification',1)}}</h6>

        <div class="heading-elements">

        </div>
    </div>
{{--  {{ dd($data) }}  --}}
    {!! Form::open(array('url' => url('pushnotif/'.$data[0]->id.'/update'), 'method' => 'post', 'class' => 'form-horizontal')) !!}
    <div class="panel-body">
        <div class="form-group">
            {!! Form::label('title',trans_choice('Title',1),array('class'=>'col-sm-3 control-label')) !!}
            <div class="col-sm-9">
                {!! Form::textarea('title',$data[0]->title, array('class' => 'form-control', 'rows'=>"4")) !!}
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('message',trans_choice('Message',1),array('class'=>'col-sm-3 control-label')) !!}
            <div class="col-sm-9">
                {!! Form::textarea('message',$data[0]->message, array('class' => 'form-control', 'rows'=>"4")) !!}
            </div>
        </div>

    </div>
    <!-- /.panel-body -->
    <div class="panel-footer">
        <button type="submit" class="btn btn-primary pull-right">{{trans_choice('general.save',1)}}</button>
    </div>
    {!! Form::close() !!}
</div>
<!-- /.box -->
@endsection

